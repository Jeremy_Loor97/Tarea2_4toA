package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        String object_id=getIntent().getStringExtra("object_id"); //Accede al "object_id" recibido como parámetro en la actividad
        DataQuery query=DataQuery.get("item"); //permite identificar al objeto que se accederá
        query.getInBackground(object_id, new GetCallback<DataObject>() {// permite que al acceder hacia atrás este vuelva a la lista de objetos

            @Override
            public void done(DataObject object, DataException e) {//Accede a las propiedades del objeto
                if(e==null){
                    TextView Nombre = (TextView)findViewById(R.id.nombre);  // Indentifica el dato nombre de tipo TextView
                    Nombre.setText(("NOMBRE: ")+(String) object.get("name")); //Muestra el texto "nombre" y el contenido del nombre que está guardado

                    ImageView  Imagen=  (ImageView)findViewById(R.id.thumbnail); //Indentificala Imagen de tipo ImageView
                    Imagen.setImageBitmap((Bitmap) object.get("image")); //Muestra la imagen

                    TextView precio=(TextView)findViewById(R.id.precio);
                    precio.setText((("PRECIO: ")+object.get("price")+getString(R.string.signo_dolar))); //Muestra el contenido del precio guardado

                    TextView Descripcion=(TextView)findViewById(R.id.descripcion); //Identifica el dato de la descripcion
                    Descripcion.setText((String) object.get("description")); //Muestra el contenido de la descripción
                    Descripcion.setMovementMethod(new ScrollingMovementMethod()); //permite que el contenido de la descripcion pueda adaptarse y que se pueda mover mediante una barra espaciadora

                }else{
                    // Error
                }
            }
        });

        // FIN - CODE6

    }

}
